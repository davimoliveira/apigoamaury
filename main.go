package main

import (
	"apiTasks/middlewares"
	"apiTasks/routes"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func majorRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome")
}

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", majorRoute)
	router.HandleFunc("/newTasks", routes.NewTask)
	router.HandleFunc("/tasks", routes.GetTasks)
	router.HandleFunc("/tasks/{id}", routes.GetTaskById)
	router.HandleFunc("/updateTask/{id}", routes.UpdateTaskById)
	router.HandleFunc("/deleteTask/{id}", routes.DeleteTaskById)
}

func main() {
	var router *mux.Router

	router = mux.NewRouter()

	router.Use(middlewares.JsonMiddleware)

	setRoutes(router)

	err := http.ListenAndServe(":333", router)
	if err != nil {
		fmt.Println("Error", err)
	}
}
